#! /bin/bash

#to install httpd service 
sudo yum -y install httpd
sudo service httpd start
sudo service httpd status

#install java8 tar file

sudo wget http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/jdk-8u131-linux-x64.tar.gz --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie"

# to install java
sudo yum -y update
sudo yum -y install java-1.8.0-openjdk-devel
java -version
sudo yum install -y unzip
echo "export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk/" >> /etc/bashrc
export PATH=$PATH:$JAVA_HOME
source /etc/bashrc

#to install git latest version
sudo yum install -y curl-devel expat-devel gettext-devel openssl-devel zlib-devel
sudo yum install -y gcc perl-ExtUtils-MakeMaker
sudo yum remove -y git
cd /usr/src

sudo wget https://www.kernel.org/pub/software/scm/git/git-2.9.3.tar.gz
tar xzf git-2.9.3.tar.gz
cd git-2.9.3
sudo make prefix=/usr/local/git all
sudo make prefix=/usr/local/git install

echo "export PATH=$PATH:/usr/local/git/bin" >> /etc/bashrc
sudo ln -s /usr/local/git/bin/git /usr/bin/git
source /etc/bashrc
git version


#install gradle latest version
sudo wget https://services.gradle.org/distributions/gradle-3.1-bin.zip
mkdir /opt/gradle
sudo unzip -d /opt/gradle gradle-3.1-bin.zip
echo "export PATH=$PATH:/opt/gradle/gradle-3.1/bin" >> /etc/bashrc
source /etc/bashrc
gradle -v


#install nodejs and npm latest version
sudo wget https://nodejs.org/dist/v6.10.2/node-v6.10.2-linux-x64.tar.xz
tar xvf node-v6.10.2-linux-x64.tar.xz
mv node-v6.10.2-linux-x64 node
sudo mv node /opt/
cd /opt
ls
cd node/bin
file node
sudo update-alternatives --install /usr/bin/node node /opt/node/bin/node 100
sudo update-alternatives --install /usr/bin/nodejs nodejs /opt/node/bin/node 100
sudo update-alternatives --install /usr/bin/npm npm /opt/node/bin/npm 100
node -v
npm -v

#install nexus

sudo wget https://download.sonatype.com/nexus/professional-bundle/nexus-professional-2.14.3-02-bundle.tar.gz
sudo tar xzf nexus-professional-2.14.3-02-bundle.tar.gz
echo "export NEXUS_HOME=/home/ec2-user/nexus-professional-2.14.3-02/bin/jsw/linux-x86-64" >> /etc/bashrc
export RUN_AS_USER=root
export PATH=$PATH:$NEXUS_HOME
source /etc/bashrc
cd  nexus-professional-2.14.3-02
cd bin
sudo bash
export RUN_AS_USER=root
./nexus start
./nexus status


#Install and configure the necessary dependencies for gitlab
sudo yum -y install curl policycoreutils openssh-server openssh-clients
sudo systemctl enable sshd
sudo systemctl start sshd
sudo yum -y install postfix
sudo systemctl enable postfix
sudo systemctl start postfix
sudo firewall-cmd --permanent --add-service=http
sudo systemctl reload firewalld


#Add the GitLab package server and install the package
curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh | sudo bash
sudo yum -y install gitlab-ce


#Configure and start gitlab:
sudo gitlab-ctl reconfigure



#ssh keypair automation:

ssh-keygen -t rsa -f ~/.ssh/sample -q -N ""
#sample is the name of the key i gave here and so it creates sample and sample.pub

cat ~/.ssh/sample.pub >> ~/.ssh/authorized_keys
sudo chmod 700 ~/.ssh/sample.pub
sudo chmod 644 ~/.ssh/authorized_keys

#add user without password:

sudo adduser jenkins
sudo passwd -d jenkins

sudo mkdir /opt/jenkins
cd /opt/jenkins

# install tomcat server
sudo wget http://www-eu.apache.org/dist/tomcat/tomcat-9/v9.0.0.M20/bin/apache-tomcat-9.0.0.M20.tar.gz
tar xzf apache-tomcat-9.0.0.M20.tar.gz
cd apache-tomcat-9.0.0.M20
#cd bin
#./startup.sh
#./shutdown.sh

#install jenkins war file 
#cd apache-tomcat-9.0.0.M20
sudo rm -rf webapps
sudo mkdir webapps
cd
sudo wget https://updates.jenkins-ci.org/download/war/2.46.1/jenkins.war

sudo mv jenkins.war /home/ec2-user/apache-tomcat-9.0.0.M20/webapps
cd apache-tomcat-9.0.0.M20
cd webapps
ls
mv jenkins.war ROOT.war

echo "export JENKINS_HOME=/home/ec2-user/apache-tomcat-9.0.0.M20/webapps/jenkins" >> /etc/bashrc
echo "export PATH=$PATH:$JENKINS_HOME" >> /etc/bashrc
source /etc/bashrc

#to get password for jenkins:
java -jar ROOT.war
